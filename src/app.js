const fastify = require('fastify')({
    logger: true
})


fastify.get('/', async (request, reply) => {
    reply.type('application/json').code(200)
    return { hello: 'world' }
})

fastify.get('/api', (req, rep) => {
    rep.code(200)
    rep.send('Welcome to the api!')
})

module.exports = fastify
